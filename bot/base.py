"""Bot Module's Base."""

from pyrogram import Client

from config import Config


def _proxy():
    if Config.BOT_PROXY is None:
        return None
    hostname, port = Config.BOT_PROXY.split(':')
    return {'hostname': hostname, 'port': int(port)}


bot = Client(session_name='telesu_bot',
             api_id=Config.BOT_API_ID,
             api_hash=Config.BOT_API_HASH,
             proxy=_proxy(), workers=1)
