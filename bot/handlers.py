"""Bot Module's Handlers."""

from pyrogram import Client
from pyrogram.types import Message
from pyrogram.filters import group

from .base import bot
from .utils import store_message


@bot.on_message(group)
def store_group_message(client: Client, message: Message):
    """Get group messages and store them in database."""
    store_message(message=message)
