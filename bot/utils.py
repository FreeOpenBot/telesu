"""Bot Utility functions."""

import json

from database.collections import Group
from database.collections import Message as MessageColl
from database.collections import User as UserColl
from pymongo.errors import DuplicateKeyError
from pyrogram.types import Chat, Message, User


def store_message(message: Message):
    """Store message in Message collection."""
    group = store_group(group=message.__dict__.pop('chat'))
    user = store_user(user=message.__dict__.pop('from_user'))
    reply_to_message = handle_reply(
        reply_to_message=message.__dict__.pop('reply_to_message', {}))
    message = json.loads(f"{message}")
    print(message.keys())
    return MessageColl.insert_one({
        **message, 'chat': group,
        'from_user': user,
        'reply_to_message': reply_to_message
    })


def store_group(group: Chat):
    """Store group in Group collection."""
    try:
        group = json.loads(f"{group}")
        return Group.insert_one(group).inserted_id
    except DuplicateKeyError:
        return Group.find_one(filter={'id': group['id']})['_id']


def store_user(user: User):
    """Store user in User collection."""
    try:
        user = json.loads(str(user))
        return UserColl.insert_one(user).inserted_id
    except DuplicateKeyError:
        return UserColl.find_one(filter={'id': user['id']})['_id']


def handle_reply(reply_to_message: Message):
    """Handle replied message."""
    if not reply_to_message:
        return {}
    message = MessageColl.find_one(
        {'message_id': reply_to_message['message_id']})
    if not message:
        message = store_message(reply_to_message)
        return message.inserted_id
    return message['_id']
