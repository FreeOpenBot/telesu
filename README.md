# TeleSU

Telegram SuperUser Bot

# Deployment
1. Create and activate a `virtualenv` => `python3 -m virtualenv .venv && source .venv/bin/active`
2. Install dependencies inside `virtualenv` => `pip install -m requirements.txt`
3. Copy `.env.example` to `.env` and fill the information.
