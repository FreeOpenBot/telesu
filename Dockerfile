FROM python:3-slim-buster

WORKDIR /usr/src/app

COPY ./requirements.txt ./

RUN pip install -q\
    --no-color \
    --no-cache-dir \
    --disable-pip-version-check \
    -r requirements.txt

COPY ./ ./

CMD [ "sh", "docker-entrypoint.sh" ]