"""Application configurations."""

import os
from dotenv import load_dotenv


class Config:
    """Configuration Object."""

    load_dotenv()

    BOT_API_ID = os.getenv('BOT_API_ID')
    BOT_API_HASH = os.getenv('BOT_API_HASH')
    BOT_PROXY = os.getenv('BOT_PROXY') or None

    DATABASE_USERNAME = os.getenv('DATABASE_USERNAME', None)
    DATABASE_PASSWORD = os.getenv('DATABASE_PASSWORD', None)
    DATABASE_HOST = os.getenv('DATABASE_HOST', '127.0.0.1')
    DATABASE_PORT = os.getenv('DATABASE_PORT', 27017)
    DATABASE_NAME = os.getenv('DATABASE_NAME', 'teleSu')
    DATABASE_AUTH_SOURCE = os.getenv('DATABASE_AUTH_SOURCE') or None


class Development(Config):
    """Development Configuration Object."""

    DEBUG = True
    TESTING = False
    ENV = 'development'


class Production(Config):
    """Production Configuration Object."""

    DEBUG = False
    TESTING = False
    ENV = 'production'


class Testing(Config):
    """Testing Configuration Object."""

    DEBUG = True
    TESTING = True
    ENV = 'testing'


def get_configuration_by_name(name: str) -> Config:
    """Get configuration by name."""
    return {
        'config': Config,
        'development': Development,
        'production': Production,
        'testing': Testing,
    }.get(name.lower(), 'config')


def get_configuration_from_env():
    """Get configuration object from environment variables."""
    return get_configuration_by_name(name=os.getenv('APP_ENV', ''))
