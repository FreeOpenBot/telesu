while [ ! -f ./INITIALIZED ]; do
  sleep 2
  echo 'Please run "docker-compose --env-file .env exec bot sh" and create the session file...'
done
python app.py
