"""Database Utility Functions."""

from urllib.parse import quote_plus


def create_database_uri(host: str,
                        port: int = 27017,
                        username: str = None,
                        password: str = None,
                        auth_source: str = None) -> str:
    """Create database URI using given parameters."""
    uri = 'mongodb://'
    if username and password:
        username = quote_plus(username)
        password = quote_plus(password)
        uri += f'{username}:{password}@'
    uri += f'{host}:{port}'
    if auth_source:
        uri += f'/?authSource={auth_source}'
    return uri
