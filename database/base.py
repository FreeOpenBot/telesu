"""Database Basic Definations."""

from pymongo import MongoClient
from pymongo.database import Database

from config import Config

from .utils import create_database_uri

uri = create_database_uri(host=Config.DATABASE_HOST,
                          port=Config.DATABASE_PORT,
                          username=Config.DATABASE_USERNAME,
                          password=Config.DATABASE_PASSWORD,
                          auth_source=Config.DATABASE_AUTH_SOURCE)
client = MongoClient(uri)
db = Database(client=client, name=Config.DATABASE_NAME)
