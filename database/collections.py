"""Database Collections Definations."""

from pymongo.collection import Collection

from .base import db

User = Collection(database=db, name='users')
User.create_index('id', name='user_id_uc', unique=True)

Group = Collection(database=db, name='groups')
Group.create_index('id', name='group_id_uc', unique=True)

Message = Collection(database=db, name='messages')
Message.create_index('id', name='message_id_uc', unique=True)
